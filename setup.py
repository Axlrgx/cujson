import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='cujson',
    version='1.0.0',
    author='Alexandru Belinașu',
    author_email='alexadru.belinasu@nshift.com',
    description='Wrapper for ujson module to serialize objects',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://github.com/mike-huls/toolbox',
    project_urls={
        "UJson Source": "https://github.com/ultrajson/ultrajson"
    },
    license='MIT',
    packages=['cujson'],
    install_requires=['ujson'],
)
