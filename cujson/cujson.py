import ujson


def decode(*args, **kwargs):  # real signature unknown
    """ Converts JSON as string to dict object structure.
    Use precise_float=True to use high precision float decoder. """
    return ujson.decode(*args, **kwargs)


def dump(*args, **kwargs):  # real signature unknown
    """ Converts arbitrary object recursively into JSON file.
    Use ensure_ascii=false to output UTF-8.
    Pass in double_precision to alter the maximum digit precision of doubles.
    Set encode_html_chars=True to encode < > & as unicode escape sequences.
    Set escape_forward_slashes=False to prevent escaping / characters. """
    return ujson.dump(*args, **kwargs)


def _serialize_object(obj):
    if isinstance(obj, dict):
        data = {}
        for (k, v) in obj.items():
            data[k] = _serialize_object(v)
        return data
    elif hasattr(obj, "__iter__") and not isinstance(obj, str):
        return [_serialize_object(v) for v in obj]
    elif hasattr(obj, "__dict__"):
        data = dict([(key, _serialize_object(value))
                     for key, value in obj.__dict__.items() if not callable(value) and not key.startswith('_')])
        return data
    else:
        return obj


def dumps(*args, **kwargs):  # real signature unknown
    """ Converts arbitrary object recursively into JSON.
    Use ensure_ascii=false to output UTF-8.
    Pass in double_precision to alter the maximum digit precision of doubles.
    Set encode_html_chars=True to encode < > & as unicode escape sequences.
    Set escape_forward_slashes=False to prevent escaping / characters. """
    arbitrary_object = _serialize_object(args[0])
    return ujson.dumps(arbitrary_object, **kwargs)


def encode(*args, **kwargs):  # real signature unknown
    """ Converts arbitrary object recursively into JSON.
    Use ensure_ascii=false to output UTF-8.
    Pass in double_precision to alter the maximum digit precision of doubles.
    Set encode_html_chars=True to encode < > & as unicode escape sequences.
    Set escape_forward_slashes=False to prevent escaping / characters. """
    return ujson.encode(*args, **kwargs)


def load(*args, **kwargs):  # real signature unknown
    """ Converts JSON as file to dict object structure.
    Use precise_float=True to use high precision float decoder. """
    return ujson.load(*args, **kwargs)


def loads(*args, **kwargs):  # real signature unknown
    """ Converts JSON as string to dict object structure.
    Use precise_float=True to use high precision float decoder. """
    return ujson.loads(*args, **kwargs)


__loader__ = None

__spec__ = None
